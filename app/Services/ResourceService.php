<?php


namespace App\Services;


use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ResourceService
{

    private static function getLastActivity(){
        $unixTimestamp =  DB::table('sessions')
            ->select('last_activity')
            ->where('user_id', auth()->user()->id)
            ->first()->last_activity;

        return Carbon::createFromTimestamp($unixTimestamp)->toDateTimeString();
    }

    private function resourceTypeYield($resourceType, $level = 0){

        $mapper = [
            'mineral mine' => [5,15,30],
            'gas refinery' => [10,20,35],
        ];
        return $mapper[$resourceType][$level];
    }

    private function buildingResourceMapper($buildingType){

        $mapper = [
            'mineral mine' => 'minerals',
            'gas refinery' => 'vespene gas',
        ];
        return $mapper[$buildingType];
    }

    public function getResourcesYields() :array {
        $res = [];

        $buildings = array_keys(getCurrUser()->ownedBuildingsQty());

        foreach ($buildings as $building){
            $buildingQty = getCurrUser()->ownedBuildingsQty()[$building];
//            dump($buildingQty);
            $yield = $this->resourceTypeYield($building);

            $res[$building] = ['qty' => $buildingQty,'yield' => $yield];
        }

        $totalResourceQty = array_map(static fn($val,$key) => $val['qty']*$val['yield'],$res,array_keys($res));
        return array_combine(array_keys($res),$totalResourceQty);
}

    public function updateResources(Request $request){

        dump($request->session()->all());
        $seconds =  $request->session()->get('secondsOverhead',0);

        $lastActivity = self::getLastActivity();

        $diff = Carbon::now()->addSeconds($seconds)->diffInSeconds($lastActivity);

        $minutes = (int)floor($diff/60);
        $extraSeconds = $diff%60;
        $request->session()->put('secondsOverhead',$extraSeconds);

        foreach (getCurrUser()->ownedBuildingsQty() as $building => $qty){

            $resourceModel = getCurrUser()->resources->filter(function($r) use($building) {

                $requiredRes = $this->buildingResourceMapper($building);

                return $r->type === $requiredRes;
            });

            $yieldPerMin = $this->resourceTypeYield($building);
            $toAdd = $minutes * $yieldPerMin * $qty;
            $oldVal = $resourceModel->first()->pivot->qty;
            $newVal = $oldVal + $toAdd;

            getCurrUser()->resources()->updateExistingPivot($resourceModel->first()->id,['qty' => $newVal]);

        }
    }

}
