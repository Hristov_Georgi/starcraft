
<?php

use App\Models\User;
use App\Services\ResourceService;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\Collection;


function getCurrUser(){
    return User::find(Auth::id());
//    return auth()->user(); //wont refresh accordingly
}
/**
 * Create a new controller instance.
 * @return Collection
 */

function getCurrUserResources(){
    return getCurrUser()->resources->pluck('pivot.qty','type');
}


function getCurrUserBuildings(){
    return getCurrUser()->buildings->pluck('pivot.qty','type');
}

function getResourcesYields() {
    $rs = new ResourceService();
    return  $rs->getResourcesYields();
}
