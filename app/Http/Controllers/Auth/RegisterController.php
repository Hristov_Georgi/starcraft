<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Building;
use App\Models\Resource;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        $mineralMineBuilding = Building::where('type', 'mineral mine')->get()->first();
        $gasRafineryBuilding = Building::where('type', 'gas refinery')->get()->first();

        $mineralResource = Resource::where('type','minerals')->get()->first();
        $gasResource = Resource::where('type','vespene gas')->get()->first();

        $mineralResource->building()->associate($mineralMineBuilding);
        $mineralResource->save();
        $gasResource->building()->associate($gasRafineryBuilding);
        $gasResource->save();


        $user->buildings()->attach($mineralMineBuilding,['qty' => 1]);
        $user->buildings()->attach($gasRafineryBuilding,['qty' => 0]);
        $user->resources()->attach($mineralResource,['qty' => 500]);
        $user->resources()->attach($gasResource,['qty' => 0]);

        return $user;
    }
}
