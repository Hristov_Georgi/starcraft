<?php

namespace App\Http\Controllers;

use App\Models\Building;
use App\Models\Resource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShopController extends Controller
{

//    public function __construct()
//    {
//        $this->middleware('auth');
//    }


    public function index()
    {
        return view('shop.index',['buildings' => Building::all()]);
    }

    public function update($buildingId)
    {
        $building = Building::find($buildingId);
        $requiredAmount = $building->cost;
        $requiredType = $building->resource->type;

        $requiredResourceTypeId = Resource::where('type',$requiredType)->get()->first()->id;


        if ($requiredAmount <= getCurrUserResources()['minerals'] ) //TODO: static currency
        {
            $userHasBuilding = getCurrUser()
                ->buildings()
                ->where('user_id',getCurrUser()->id)
                ->where('building_id',$buildingId)
                ->get();

            if (count($userHasBuilding) == 0)
            {
                getCurrUser()->buildings()->attach($buildingId,['qty'=> '1']);
            } else {
                getCurrUser()->buildings()->where('type',$building->type)->first()->pivot->increment('qty');
            }

            $amountLeft = getCurrUserResources()['minerals'] - $requiredAmount ;

            getCurrUser()->resources()->updateExistingPivot(getCurrUser()->resources()->where('resource_id',1)->get(), [ //TODO:hardcoded value for currency
                'qty' => $amountLeft,
            ]);



            return redirect()->route('home')->with('message','purchase successful');
        }

        return back()->with('message','insufficient funds');
    }
}
