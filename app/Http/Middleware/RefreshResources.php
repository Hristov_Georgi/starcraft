<?php

namespace App\Http\Middleware;

use App\Services\ResourceService;
use Closure;
use Illuminate\Http\Request;

class RefreshResources
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $instance = new ResourceService();
        $instance->updateResources($request);

        return $next($request);
    }
}
