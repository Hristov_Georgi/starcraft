<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Building extends Model
{
    use HasFactory;

    protected $fillable = ['type'];

    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot('qty');
    }

    public function resource()
    {
        return $this->hasOne(Resource::class);
    }
    public function getResourceName()
    {
        return Str::of($this->resource()->first()->type)->title();
    }

}
