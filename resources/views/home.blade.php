@extends('layouts.app')

@section('content')
<div class="container">
@if(Session::has('message'))
    <p class="alert alert-success">{{ Session::get('message') }}</p>
@endif
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Buildings') }}</div>

                <div class="card-body">
{{--                    @if (session('status'))--}}
{{--                        <div class="alert alert-success" role="alert">--}}
{{--                            {{ session('status') }}--}}
{{--                        </div>--}}
{{--                    @endif--}}
{{--                    --}}{{-- {{ __('You are logged in!') }} --}}
                    <div class="py-1">
                        <img style="width: 50px" height="85px" src="{{ asset('images/mineral_mine2.jpg') }}" alt=""><span>{{ $buildings['mineral mine'] }} QTY</span>
                    </div>
                    <div class="py-1">
                        <img style="width: 50px" height="85px" src="{{ asset('images/refinery2.jpg') }}" alt=""><span> {{ $buildings['gas refinery'] }} QTY</span>
                    </div>

                </div>
            </div>
        </div>

        <div class="card">
            <div  class="card-header">{{ __('Dashboard') }}</div>
            <div class="card-body">
                <div class="py-1">
                    <img style="width: 50px" height="85px" src="{{ asset('images/minerals2.png') }}" alt="">
                    <div class="text-center d-inline-block">
                        <div class="m-1 ">{{ $resources['minerals'] }} QTY</div>
                        <div class="m-1">Yield: {{ getResourcesYields()['mineral mine']  }}/min</div>
                    </div>
                </div>
                <div class="py-1">
                    <img style="width: 50px" height="85px" src="{{ asset('images/gas.jpg') }}" alt="">
                    <div class="text-center d-inline-block">
                        <div class="m-1 ">{{ $resources['vespene gas'] }} QTY</div>
                        <div class="m-1">Yield: {{ getResourcesYields()['gas refinery'] ?? 0 }}/min</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
