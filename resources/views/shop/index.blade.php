

@extends('layouts.app')


@section('content')



<div class="container">
    @if(Session::has('message'))
        <p class="alert alert-danger">{{ Session::get('message') }}</p>
    @endif
    <h1>Shop</h1>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Building</th>
            <th scope="col">Price</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach( $buildings as $key => $building)
            <tr>
                <th scope="row">{{ ++$key }}</th>
                <td>{{$building->type}}</td>
                <td>{{$building->cost}}</td>
                <td>
                    <form action="{{ route('shop.update',$building->id) }}" method="post">
                        @csrf
                        @method('put')
                        <button type="submit">Buy</button>
                    </form>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
</div>

@endsection
