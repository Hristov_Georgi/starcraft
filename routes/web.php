<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ShopController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Auth::routes();



Route::group(['middleware' => ['auth','refresh.resources']], function(){
    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::resource('shop', ShopController::class);

});


Route::get('/guestlogin', function (){
    Auth::loginUsingId(1);
    return redirect()->route('home');
});

Route::get('/guestlogout', function (){
    Auth::logout();

    request()->session()->invalidate();

    request()->session()->regenerateToken();

    return redirect('/');
});
